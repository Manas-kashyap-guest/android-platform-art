NAME = dmtracedump
SOURCES = tracedump.cc
SOURCES := $(foreach source, $(SOURCES), tools/dmtracedump/$(source))
CPPFLAGS += -Itools/dmtracedump

debian/out/$(NAME): $(SOURCES)
	mkdir --parents debian/out
	$(CXX) $^ -o $@ $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)