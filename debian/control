Source: android-platform-art
Section: devel
Priority: optional
Maintainer: Android Tools Maintainers <android-tools-devel@lists.alioth.debian.org>
Uploaders: Kai-Chung Yan <seamlik@debian.org>
Build-Depends:
 android-libbacktrace-dev (>= 1:8.1.0+r23) [amd64 i386 armel armhf arm64],
 android-libbase-dev (>= 1:8.1.0+r23) [amd64 i386 armel armhf arm64],
 android-libcutils-dev (>= 1:8.1.0+r23-3~) [amd64 i386 armel armhf arm64],
 android-libnativebridge-dev (>= 1:8.1.0+r23) [amd64 i386 armel armhf arm64],
 android-libnativeloader-dev (>= 1:8.1.0+r23) [amd64 i386 armel armhf arm64],
 android-libutils-dev (>= 1:8.1.0+r23) [amd64 i386 armel armhf arm64],
 android-libziparchive-dev (>= 1:8.1.0+r23) [amd64 i386 armel armhf arm64],
 clang [amd64 i386 armel armhf arm64],
 debhelper-compat (= 12),
 dh-exec,
 help2man,
 libicu-dev [amd64 i386 armel armhf arm64],
 liblz4-dev [amd64 i386 armel armhf arm64],
 python3 [amd64 i386 armel armhf arm64],
 valgrind [amd64 i386 armel armhf arm64],
 zlib1g-dev [amd64 i386 armel armhf arm64]
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://android.googlesource.com/platform/art
Vcs-Git: https://salsa.debian.org/android-tools-team/android-platform-art.git
Vcs-Browser: https://salsa.debian.org/android-tools-team/android-platform-art

Package: dexdump
Architecture: amd64 i386 armel armhf arm64
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Displays information about Android DEX files
 The `dexdump` tool is intended to mimic `objdump`. When possible, use similar
 command-line arguments.
 .
 This is a re-implementation of the original `dexdump` utility that was
 based on Dalvik functions in `libdex` into a new `dexdump` that is now
 based on ART functions in `libart` instead. The output is very similar to
 to the original for correct DEX files. Error messages may differ, however.
 Also, ODEX files are no longer supported.

Package: dmtracedump
Architecture: any
Multi-Arch: foreign
Depends: graphviz, ${shlibs:Depends}, ${misc:Depends}
Description: Generates graphical call-stack diagrams from Android trace logs
 `dmtracedump` generates the call stack data as a tree diagram, where each node
 represents a method call. It shows call flow (from parent node to child nodes)
 using arrows.

Package: android-libart
Architecture: amd64 i386 armel armhf arm64
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Android Runtime
 Android Runtime (ART) is the managed runtime used by applications and some
 system services on Android. ART and its predecessor Dalvik were originally
 created specifically for the Android project. ART as the runtime executes the
 Dalvik Executable format and DEX bytecode specification.
 .
 This package provides `libart` and `libsigchain`.
 .
 This library is only used by Android SDK and uses a customized RPATH.

Package: dexlist
Architecture: amd64 i386 armel armhf arm64
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Lists all methods in all concrete classes in Android DEX files
 This is a re-implementation of the original `dexlist` utility that was
 based on Dalvik functions in `libdex` into a new `dexlist` that is now
 based on ART functions in `libart` instead. The output is very similar to
 to the original for correct DEX files. Error messages may differ, however.
 Also, ODEX files are no longer supported.
