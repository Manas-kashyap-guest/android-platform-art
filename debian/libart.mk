NAME = libart

SOURCES = \
  aot_class_linker.cc \
  art_field.cc \
  art_method.cc \
  atomic.cc \
  barrier.cc \
  base/allocator.cc \
  base/arena_allocator.cc \
  base/arena_bit_vector.cc \
  base/bit_vector.cc \
  base/file_magic.cc \
  base/hex_dump.cc \
  base/logging.cc \
  base/mutex.cc \
  base/safe_copy.cc \
  base/scoped_arena_allocator.cc \
  base/scoped_flock.cc \
  base/stringpiece.cc \
  base/time_utils.cc \
  base/timing_logger.cc \
  base/unix_file/fd_file.cc \
  base/unix_file/random_access_file_utils.cc \
  cha.cc \
  check_jni.cc \
  class_linker.cc \
  class_loader_context.cc \
  class_table.cc \
  code_simulator_container.cc \
  common_throws.cc \
  compiler_filter.cc \
  debugger.cc \
  dex_file.cc \
  dex_file_annotations.cc \
  dex_file_layout.cc \
  dex_file_tracking_registrar.cc \
  dex_file_verifier.cc \
  dex_instruction.cc \
  dex_to_dex_decompiler.cc \
  elf_file.cc \
  exec_utils.cc \
  fault_handler.cc \
  gc/allocation_record.cc \
  gc/allocator/dlmalloc.cc \
  gc/allocator/rosalloc.cc \
  gc/accounting/bitmap.cc \
  gc/accounting/card_table.cc \
  gc/accounting/heap_bitmap.cc \
  gc/accounting/mod_union_table.cc \
  gc/accounting/remembered_set.cc \
  gc/accounting/space_bitmap.cc \
  gc/collector/concurrent_copying.cc \
  gc/collector/garbage_collector.cc \
  gc/collector/immune_region.cc \
  gc/collector/immune_spaces.cc \
  gc/collector/mark_compact.cc \
  gc/collector/mark_sweep.cc \
  gc/collector/partial_mark_sweep.cc \
  gc/collector/semi_space.cc \
  gc/collector/sticky_mark_sweep.cc \
  gc/gc_cause.cc \
  gc/heap.cc \
  gc/reference_processor.cc \
  gc/reference_queue.cc \
  gc/scoped_gc_critical_section.cc \
  gc/space/bump_pointer_space.cc \
  gc/space/dlmalloc_space.cc \
  gc/space/image_space.cc \
  gc/space/large_object_space.cc \
  gc/space/malloc_space.cc \
  gc/space/region_space.cc \
  gc/space/rosalloc_space.cc \
  gc/space/space.cc \
  gc/space/zygote_space.cc \
  gc/task_processor.cc \
  gc/verification.cc \
  hprof/hprof.cc \
  image.cc \
  indirect_reference_table.cc \
  instrumentation.cc \
  intern_table.cc \
  interpreter/interpreter.cc \
  interpreter/interpreter_common.cc \
  interpreter/interpreter_intrinsics.cc \
  interpreter/interpreter_switch_impl.cc \
  interpreter/lock_count_data.cc \
  interpreter/shadow_frame.cc \
  interpreter/unstarted_runtime.cc \
  java_frame_root_info.cc \
  java_vm_ext.cc \
  jdwp/jdwp_event.cc \
  jdwp/jdwp_expand_buf.cc \
  jdwp/jdwp_handler.cc \
  jdwp/jdwp_main.cc \
  jdwp/jdwp_request.cc \
  jdwp/jdwp_socket.cc \
  jdwp/object_registry.cc \
  jni_env_ext.cc \
  jit/debugger_interface.cc \
  jit/jit.cc \
  jit/jit_code_cache.cc \
  jit/profile_compilation_info.cc \
  jit/profiling_info.cc \
  jit/profile_saver.cc \
  jni_internal.cc \
  jobject_comparator.cc \
  linear_alloc.cc \
  managed_stack.cc \
  mem_map.cc \
  memory_region.cc \
  method_handles.cc \
  mirror/array.cc \
  mirror/call_site.cc \
  mirror/class.cc \
  mirror/class_ext.cc \
  mirror/dex_cache.cc \
  mirror/emulated_stack_frame.cc \
  mirror/executable.cc \
  mirror/field.cc \
  mirror/method.cc \
  mirror/method_handle_impl.cc \
  mirror/method_handles_lookup.cc \
  mirror/method_type.cc \
  mirror/object.cc \
  mirror/reference.cc \
  mirror/stack_trace_element.cc \
  mirror/string.cc \
  mirror/throwable.cc \
  monitor.cc \
  native_bridge_art_interface.cc \
  native_stack_dump.cc \
  native/dalvik_system_DexFile.cc \
  native/dalvik_system_VMDebug.cc \
  native/dalvik_system_VMRuntime.cc \
  native/dalvik_system_VMStack.cc \
  native/dalvik_system_ZygoteHooks.cc \
  native/java_lang_Class.cc \
  native/java_lang_Object.cc \
  native/java_lang_String.cc \
  native/java_lang_StringFactory.cc \
  native/java_lang_System.cc \
  native/java_lang_Thread.cc \
  native/java_lang_Throwable.cc \
  native/java_lang_VMClassLoader.cc \
  native/java_lang_Void.cc \
  native/java_lang_invoke_MethodHandleImpl.cc \
  native/java_lang_ref_FinalizerReference.cc \
  native/java_lang_ref_Reference.cc \
  native/java_lang_reflect_Array.cc \
  native/java_lang_reflect_Constructor.cc \
  native/java_lang_reflect_Executable.cc \
  native/java_lang_reflect_Field.cc \
  native/java_lang_reflect_Method.cc \
  native/java_lang_reflect_Parameter.cc \
  native/java_lang_reflect_Proxy.cc \
  native/java_util_concurrent_atomic_AtomicLong.cc \
  native/libcore_util_CharsetUtils.cc \
  native/org_apache_harmony_dalvik_ddmc_DdmServer.cc \
  native/org_apache_harmony_dalvik_ddmc_DdmVmInternal.cc \
  native/sun_misc_Unsafe.cc \
  non_debuggable_classes.cc \
  oat.cc \
  oat_file.cc \
  oat_file_assistant.cc \
  oat_file_manager.cc \
  oat_quick_method_header.cc \
  object_lock.cc \
  offsets.cc \
  os_linux.cc \
  parsed_options.cc \
  plugin.cc \
  primitive.cc \
  quick_exception_handler.cc \
  read_barrier.cc \
  reference_table.cc \
  reflection.cc \
  runtime.cc \
  runtime_callbacks.cc \
  runtime_common.cc \
  runtime_options.cc \
  scoped_thread_state_change.cc \
  signal_catcher.cc \
  stack.cc \
  stack_map.cc \
  thread.cc \
  thread_list.cc \
  thread_pool.cc \
  ti/agent.cc \
  trace.cc \
  transaction.cc \
  type_lookup_table.cc \
  utf.cc \
  utils.cc \
  vdex_file.cc \
  verifier/instruction_flags.cc \
  verifier/method_verifier.cc \
  verifier/reg_type.cc \
  verifier/reg_type_cache.cc \
  verifier/register_line.cc \
  verifier/verifier_deps.cc \
  verify_object.cc \
  well_known_classes.cc \
  zip_archive.cc \
  arch/context.cc \
  arch/instruction_set.cc \
  arch/instruction_set_features.cc \
  arch/memcmp16.cc \
  arch/arm/instruction_set_features_arm.cc \
  arch/arm/registers_arm.cc \
  arch/arm64/instruction_set_features_arm64.cc \
  arch/arm64/registers_arm64.cc \
  arch/mips/instruction_set_features_mips.cc \
  arch/mips/registers_mips.cc \
  arch/mips64/instruction_set_features_mips64.cc \
  arch/mips64/registers_mips64.cc \
  arch/x86/instruction_set_features_x86.cc \
  arch/x86/registers_x86.cc \
  arch/x86_64/registers_x86_64.cc \
  entrypoints/entrypoint_utils.cc \
  entrypoints/jni/jni_entrypoints.cc \
  entrypoints/math_entrypoints.cc \
  entrypoints/quick/quick_alloc_entrypoints.cc \
  entrypoints/quick/quick_cast_entrypoints.cc \
  entrypoints/quick/quick_deoptimization_entrypoints.cc \
  entrypoints/quick/quick_dexcache_entrypoints.cc \
  entrypoints/quick/quick_entrypoints_enum.cc \
  entrypoints/quick/quick_field_entrypoints.cc \
  entrypoints/quick/quick_fillarray_entrypoints.cc \
  entrypoints/quick/quick_jni_entrypoints.cc \
  entrypoints/quick/quick_lock_entrypoints.cc \
  entrypoints/quick/quick_math_entrypoints.cc \
  entrypoints/quick/quick_thread_entrypoints.cc \
  entrypoints/quick/quick_throw_entrypoints.cc \
  entrypoints/quick/quick_trampoline_entrypoints.cc
# Sources for a host library
SOURCES += \
  monitor_linux.cc \
  runtime_linux.cc \
  thread_linux.cc

# Architecture specific sources
SOURCES_arm = \
  interpreter/mterp/mterp.cc \
  interpreter/mterp/out/mterp_arm.S \
  arch/arm/context_arm.cc \
  arch/arm/entrypoints_init_arm.cc \
  arch/arm/instruction_set_features_assembly_tests.S \
  arch/arm/jni_entrypoints_arm.S \
  arch/arm/memcmp16_arm.S \
  arch/arm/quick_entrypoints_arm.S \
  arch/arm/quick_entrypoints_cc_arm.cc \
  arch/arm/thread_arm.cc \
  arch/arm/fault_handler_arm.cc
SOURCES_arm64 = \
  interpreter/mterp/mterp.cc \
  interpreter/mterp/out/mterp_arm64.S \
  arch/arm64/context_arm64.cc \
  arch/arm64/entrypoints_init_arm64.cc \
  arch/arm64/jni_entrypoints_arm64.S \
  arch/arm64/memcmp16_arm64.S \
  arch/arm64/quick_entrypoints_arm64.S \
  arch/arm64/thread_arm64.cc \
  monitor_pool.cc \
  arch/arm64/fault_handler_arm64.cc
SOURCES_x86 = \
  interpreter/mterp/mterp.cc \
  interpreter/mterp/out/mterp_x86.S \
  arch/x86/context_x86.cc \
  arch/x86/entrypoints_init_x86.cc \
  arch/x86/jni_entrypoints_x86.S \
  arch/x86/memcmp16_x86.S \
  arch/x86/quick_entrypoints_x86.S \
  arch/x86/thread_x86.cc \
  arch/x86/fault_handler_x86.cc
SOURCES_x86_64 = \
  interpreter/mterp/mterp.cc \
  interpreter/mterp/out/mterp_x86_64.S \
  arch/x86_64/context_x86_64.cc \
  arch/x86_64/entrypoints_init_x86_64.cc \
  arch/x86_64/jni_entrypoints_x86_64.S \
  arch/x86_64/memcmp16_x86_64.S \
  arch/x86_64/quick_entrypoints_x86_64.S \
  arch/x86_64/thread_x86_64.cc \
  monitor_pool.cc \
  arch/x86/fault_handler_x86.cc
SOURCES_mips = \
  interpreter/mterp/mterp.cc \
  interpreter/mterp/out/mterp_mips.S \
  arch/mips/context_mips.cc \
  arch/mips/entrypoints_init_mips.cc \
  arch/mips/jni_entrypoints_mips.S \
  arch/mips/memcmp16_mips.S \
  arch/mips/quick_entrypoints_mips.S \
  arch/mips/thread_mips.cc \
  arch/mips/fault_handler_mips.cc
SOURCES_mips64 = \
  interpreter/mterp/mterp.cc \
  interpreter/mterp/out/mterp_mips64.S \
  arch/mips64/context_mips64.cc \
  arch/mips64/entrypoints_init_mips64.cc \
  arch/mips64/jni_entrypoints_mips64.S \
  arch/mips64/memcmp16_mips64.S \
  arch/mips64/quick_entrypoints_mips64.S \
  arch/mips64/thread_mips64.cc \
  monitor_pool.cc \
  arch/mips64/fault_handler_mips64.cc
include debian/detect-arch.mk
SOURCES += $(SOURCES_$(CPU))

SOURCES := $(foreach source, $(SOURCES), runtime/$(source))

# operator_out.cc
SOURCES += operator_out.cc
SOURCES_OPERATOR = \
  arch/instruction_set.h \
  base/allocator.h \
  base/callee_save_type.h \
  base/enums.h \
  base/mutex.h \
  debugger.h \
  base/unix_file/fd_file.h \
  dex_file.h \
  dex_file_layout.h \
  dex_instruction.h \
  dex_instruction_utils.h \
  gc_root.h \
  gc/allocator_type.h \
  gc/allocator/rosalloc.h \
  gc/collector_type.h \
  gc/collector/gc_type.h \
  gc/heap.h \
  gc/space/region_space.h \
  gc/space/space.h \
  gc/weak_root_state.h \
  image.h \
  instrumentation.h \
  indirect_reference_table.h \
  invoke_type.h \
  jdwp/jdwp.h \
  jdwp/jdwp_constants.h \
  lock_word.h \
  mirror/class.h \
  oat.h \
  object_callbacks.h \
  process_state.h \
  stack.h \
  suspend_reason.h \
  thread.h \
  thread_state.h \
  ti/agent.h \
  verifier/verifier_enums.h
SOURCES_OPERATOR := $(foreach source, $(SOURCES_OPERATOR), runtime/$(source))

# If directly compile all sources together, it will take a huge amount of time
# and potentially huge amount of memory. Not good for devel-time.
# On the other hand, individual compiling every source allows incremental
# compilation and multi-thread accelration.
OBJECTS_ASSEMBLY = $(SOURCES_ASSEMBLY:.S=.o)
OBJECTS_CXX = $(SOURCES_CXX:.cc=.o)
SOURCES_ASSEMBLY = $(filter %.S,$(SOURCES))
SOURCES_CXX = $(filter %.cc,$(SOURCES))

CFLAGS += -c
CXXFLAGS += -c

CPPFLAGS += \
  -DART_BASE_ADDRESS_MAX_DELTA=0x1000000 \
  -DART_BASE_ADDRESS_MIN_DELTA=-0x1000000 \
  -DART_BASE_ADDRESS=0x60000000 \
  -DART_DEFAULT_COMPACT_DEX_LEVEL=fast \
  -DART_DEFAULT_GC_TYPE_IS_CMS \
  -DART_ENABLE_ADDRESS_SANITIZER=1 \
  -DART_ENABLE_CODEGEN_arm \
  -DART_ENABLE_CODEGEN_arm64 \
  -DART_ENABLE_CODEGEN_mips \
  -DART_ENABLE_CODEGEN_mips64 \
  -DART_ENABLE_CODEGEN_x86 \
  -DART_ENABLE_CODEGEN_x86_64 \
  -DART_FRAME_SIZE_LIMIT=6400 \
  -DART_READ_BARRIER_TYPE_IS_BAKER=1 \
  -DART_STACK_OVERFLOW_GAP_arm=8192 \
  -DART_STACK_OVERFLOW_GAP_arm64=8192 \
  -DART_STACK_OVERFLOW_GAP_mips=16384 \
  -DART_STACK_OVERFLOW_GAP_mips64=16384 \
  -DART_STACK_OVERFLOW_GAP_x86_64=20480 \
  -DART_STACK_OVERFLOW_GAP_x86=16384 \
  -DART_USE_READ_BARRIER=1 \
  -DBUILDING_LIBART=1 \
  -DIMT_SIZE=43 \
  -DUSE_D8_DESUGAR=1 \
  -I. \
  -I/usr/include/android/nativehelper \
  -I/usr/include/valgrind \
  -Icmdline \
  -Iruntime \
  -Iruntime/generated \
  -Isigchainlib
LDFLAGS += \
  -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Ldebian/out \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -shared -Wl,-soname,$(NAME).so.0
LIBRARIES_FLAGS = \
  -lbacktrace \
  -lbase \
  -lcutils \
  -ldl \
  -llz4 \
  -lnativebridge \
  -lnativehelper \
  -lnativeloader \
  -lpthread \
  -lsigchain \
  -lz \
  -lziparchive

CC_ASSEMBLY = clang
ifeq ($(CPU),arm)
  # Clang does not suppirt the `ADRL` instruction. See <https://bugs.llvm.org/show_bug.cgi?id=24350>
  CC_ASSEMBLY = gcc
endif

debian/out/$(NAME).so.0: $(OBJECTS_CXX) $(OBJECTS_ASSEMBLY)
	mkdir --parents debian/out
	$(CXX) -o $@ $(LDFLAGS) $^ $(LIBRARIES_FLAGS)
	ln -s $(NAME).so.0 debian/out/$(NAME).so

clean:
	$(RM) $(NAME).so* operator_out.cc
	$(RM) $(OBJECTS_CXX)
	$(RM) $(OBJECTS_ASSEMBLY)

.PHONY: clean

$(OBJECTS_CXX): %.o: %.cc
	clang++ -o $@ $(CXXFLAGS) $(CPPFLAGS) $^

$(OBJECTS_ASSEMBLY): %.o: %.S
	$(CC_ASSEMBLY) -o $@ $(CFLAGS) $(CPPFLAGS) $^

operator_out.cc: $(SOURCES_OPERATOR)
	python3 tools/generate-operator-out.py runtime $^ > $@